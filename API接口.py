import requests
import re,time

url = "https://cdn.heweather.com/china-city-list.txt"

res = requests.get(url)

data = res.content.decode("utf-8")


dlist = re.split("[\n\r]+",data)

for a in range(5):
    dlist.remove(dlist[0])
for i in range(2):
    #使用空白符拆分出每个字段信息
    item = re.split("\s+",dlist[i])
    #输出
    #print(item)
    #print(item[1],":",item[3]," ",item[5])
    url = "https://free-api.heweather.com/s6/weather?location=%s&key=4efa3cc470704500980d345a962fbe5c"%(item[5])
    res = requests.get(url)
    time.sleep(2)
    datalist = res.json()
    data = datalist['HeWeather6'][0]
    #输出天气信息
    print("城市：", data['basic']['location'])
    print("时间：", str(data['daily_forecast'][0]['date']))
    print("温度：", data["daily_forecast"][0]["tmp_min"],'~',data["daily_forecast"][0]["tmp_max"])
    print(data['daily_forecast'][0]['cond_txt_d']," . ",data['daily_forecast'][0]['cond_txt_n'])
    print(data['daily_forecast'][0]['wind_dir'], data['daily_forecast'][0]['wind_sc'], '级')
    print('建议：',data["lifestyle"][0]["txt"])
    print("-"*60)