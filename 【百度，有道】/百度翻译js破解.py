'''百度翻译js破解'''

import execjs
import requests
import json

def fanyi(query):
    '''在调试工具中找到加密生成参数的代码'''
    jsCode = """
    function n(r, o) {
            for (var t = 0; t < o.length - 2; t += 3) {
                var a = o.charAt(t + 2);
                a = a >= "a" ? a.charCodeAt(0) - 87 : Number(a),
                a = "+" === o.charAt(t + 1) ? r >>> a : r << a,
                r = "+" === o.charAt(t) ? r + a & 4294967295 : r ^ a
            }
            return r
        }
        var i = null;
        function e(r) {
            var o = r.match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g);
            if (null === o) {
                var t = r.length;
                t > 30 && (r = "" + r.substr(0, 10) + r.substr(Math.floor(t / 2) - 5, 10) + r.substr(-10, 10))
            } else {
                for (var e = r.split(/[\uD800-\uDBFF][\uDC00-\uDFFF]/), C = 0, h = e.length, f = []; h > C; C++)
                    "" !== e[C] && f.push.apply(f, a(e[C].split(""))),
                    C !== h - 1 && f.push(o[C]);
                var g = f.length;
                g > 30 && (r = f.slice(0, 10).join("") + f.slice(Math.floor(g / 2) - 5, Math.floor(g / 2) + 5).join("") + f.slice(-10).join(""))
            }
            var u = void 0
              , l = "" + String.fromCharCode(103) + String.fromCharCode(116) + String.fromCharCode(107);
            u = null !== i ? i : (i = '320305.131321201' || "") || "";
            for (var d = u.split("."), m = Number(d[0]) || 0, s = Number(d[1]) || 0, S = [], c = 0, v = 0; v < r.length; v++) {
                var A = r.charCodeAt(v);
                128 > A ? S[c++] = A : (2048 > A ? S[c++] = A >> 6 | 192 : (55296 === (64512 & A) && v + 1 < r.length && 56320 === (64512 & r.charCodeAt(v + 1)) ? (A = 65536 + ((1023 & A) << 10) + (1023 & r.charCodeAt(++v)),
                S[c++] = A >> 18 | 240,
                S[c++] = A >> 12 & 63 | 128) : S[c++] = A >> 12 | 224,
                S[c++] = A >> 6 & 63 | 128),
                S[c++] = 63 & A | 128)
            }
            for (var p = m, F = "" + String.fromCharCode(43) + String.fromCharCode(45) + String.fromCharCode(97) + ("" + String.fromCharCode(94) + String.fromCharCode(43) + String.fromCharCode(54)), D = "" + String.fromCharCode(43) + String.fromCharCode(45) + String.fromCharCode(51) + ("" + String.fromCharCode(94) + String.fromCharCode(43) + String.fromCharCode(98)) + ("" + String.fromCharCode(43) + String.fromCharCode(45) + String.fromCharCode(102)), b = 0; b < S.length; b++)
                p += S[b],
                p = n(p, F);
            return p = n(p, D),
            p ^= s,
            0 > p && (p = (2147483647 & p) + 2147483648),
            p %= 1e6,
            p.toString() + "." + (p ^ m)
        }
    """
    # query = input('请输入你要翻译的东西：')
    sign = execjs.compile(jsCode).call('e', query)

    data = {
        'from':'zh',                                   #   在此处修改参数可以翻译英文       zh  改为 en
        'to':'en',                                      #   en   改为 zh
        'query':query,
        'transtype':'transtime',                        #    transtime    改为   translang
        'simple_means_flag':'3',
        'sign':sign,
        'token':'c19ce1b90ec11ab8764e2edc698743d8',
    }
    headers = {
        'accept':'*/*',
        'accept-encoding':'gzip, deflate, br',
        'accept-language':'zh-CN,zh;q=0.9',
        'cache-control':'no-cache',
        'content-length':'143',
        'content-type':'application/x-www-form-urlencoded; charset=UTF-8',
        'origin':'https://fanyi.baidu.com',
        'pragma':'no-cache',
        'referer':'https://fanyi.baidu.com/translate?aldtype=16047&query=&keyfrom=baidu&smartresult=dict&lang=auto2zh',
        'sec-fetch-mode':'cors',
        'sec-fetch-site':'same-origin',
        'user-agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
        'x-requested-with':'XMLHttpRequest',
        'cookie':'BAIDUID=AF23D77D34FCD8C34768088EA68389F7:FG=1;BIDUPSID=AF23D77D34FCD8C34768088EA68389F7;PSTM=1564560576;REALTIME_TRANS_SWITCH=1;FANYI_WORD_SWITCH=1;HISTORY_SWITCH=1;SOUND_SPD_SWITCH=1; SOUND_PREFER_SWITCH=1;APPGUIDE_8_0_0=1; MCITY=-%3A; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; locale=zh; cflag=13%3A3; Hm_lvt_64ecd82404c51e03dc91cb9e8c025574=1569308529,1569308546,1569399936,1569460437; Hm_lpvt_64ecd82404c51e03dc91cb9e8c025574=1569460437; __yjsv5_shitong=1.0_7_0a54f354ef14c14905b10ae0292fa54b74d1_300_1569460542475_113.248.149.66_bc9e5694; yjs_js_security_passport=0b57d8e9bfb54c3499281d5144fe5c617d00cfac_1569460542_js; to_lang_often=%5B%7B%22value%22%3A%22zh%22%2C%22text%22%3A%22%u4E2D%u6587%22%7D%2C%7B%22value%22%3A%22en%22%2C%22text%22%3A%22%u82F1%u8BED%22%7D%5D; from_lang_often=%5B%7B%22value%22%3A%22fra%22%2C%22text%22%3A%22%u6CD5%u8BED%22%7D%2C%7B%22value%22%3A%22en%22%2C%22text%22%3A%22%u82F1%u8BED%22%7D%2C%7B%22value%22%3A%22zh%22%2C%22text%22%3A%22%u4E2D%u6587%22%7D%5D',
    }

    url = 'https://fanyi.baidu.com/v2transapi'
    res = requests.post(url=url,data=data,headers=headers)
    # print(res.content.decode('utf-8'))
    result = json.loads(res.content.decode('utf-8')).get('trans_result').get('data')[0]
    data = dict()
    data['英文'] = result.get('dst')
    data['中文'] = result.get('src')
    print(data)

if __name__ == '__main__':
    while True:
        query = input('你要翻译的内容：')
        if query == 'q':
            break
        fanyi(query)




